﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace LearnAutomation.Steps
{
    [Binding]
    public sealed class FirstSteps
    {
        
        [When(@"I press add")]
        public void WhenIPressAdd()
        {
            ScenarioContext.Current.Pending();
        }

        [Given(@"I have entered into the calculator")]
        public void GivenIHaveEnteredIntoTheCalculator(Table table)
        {
            ScenarioContext.Current.Pending();
        }

    


        [Then(@"the result should be (.*) on the screen")]
        public void ThenTheResultShouldBeOnTheScreen(int p0)
        {
            ScenarioContext.Current.Pending();
        }

    }
}
