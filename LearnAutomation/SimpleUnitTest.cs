﻿using NUnit.Framework;

namespace LearnAutomation
{
    #region comment
    //We add NUnit packages
    //Write simple unit tests
    #endregion
    [TestFixture]
    public class SimpleUnitTest
    {
        [Test]
        public void Check_BasicOperationsInTest()
        {
            var calculator =new Calculator(); //Arrange
            var sum= calculator.Add(5,10); //Act
            Assert.AreEqual(sum, 40); //Assert
        }
    }
}
