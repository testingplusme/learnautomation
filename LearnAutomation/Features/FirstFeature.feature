﻿Feature: FirstFeature
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

@mytag
Scenario: Add two numbers
	Given I have entered into the calculator
	| Name | Surname  |
	| John | Kowalski |
	And I have entered into the calculator
	| Name | Surname  |
	| John | Kowalski |
	When I press add
	Then the result should be 120 on the screen
